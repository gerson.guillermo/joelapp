import 'package:exsawebb/register.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'dart:async' show Future;
import 'login.dart';

void main() {
  runApp(MaterialApp(
    home: MySlides(),
  ));

}

class MySlides extends StatefulWidget {
  @override
  _MySlideState createState() => _MySlideState();
}

class _MySlideState extends State<MySlides> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        color: Colors.white,
        child: Column(
          children: [
            SizedBox(
              height: 100,
            ),
            Image.asset(
                "assets/images/logo.png",
              alignment: Alignment.topCenter,
              width: 250,
            ),
            SizedBox(
              height: 20,
            ),
            Text("¡Visualiza, conecta con un emprendedor  y pide un servicio y/o producto!",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 13
              ),
            ),
            SizedBox(
              height: 60,
            ),
            FlatButton(
              color:  Color(0xFFF4FDFE),
              minWidth: 300,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25.0),
                  side: BorderSide(color: Color.fromRGBO(255, 255, 255, 1))
              ),
              onPressed: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyLogin()),
                );
              },
              child:Padding(
                padding: EdgeInsets.only(top: 13,bottom: 13),
                child: Text(
                  "INICIAR SESIÓN",
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 15,
            ),
            Text(
              "- O -",
              style: TextStyle(
                fontSize: 20
              ),
            ),
            SizedBox(
              height: 15,
            ),
            FlatButton(
              color:  Color(0xFF03BEDF),
              minWidth: 300,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25.0),
                  side: BorderSide(color: Color.fromRGBO(255, 255, 255, 1))
              ),
              onPressed: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyRegister()),
                );
              },
              child:Padding(
                padding: EdgeInsets.only(top: 13,bottom: 13),
                child: Text(
                  "REGISTRARSE",
                  style: TextStyle(
                    fontSize: 22,
                    color: Color(0xFFF9FAFC),
                  ),
                ),
              ),
            ),
          ],
        )
      ),
    );
  }

}