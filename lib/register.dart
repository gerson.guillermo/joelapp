import 'package:flutter/material.dart';


class MyRegister extends StatefulWidget {
  @override
  _MyRegisterState createState() => _MyRegisterState();
}

class _MyRegisterState extends State<MyRegister> {
  var emailController = TextEditingController(text: "");
  var passController = TextEditingController(text: "");
  var numberController = TextEditingController(text: "");
  var docController = TextEditingController(text: "");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(iconTheme: IconThemeData(

        color: Colors.black, //change your color here
      ),backgroundColor: Colors.white ,elevation: 0,),
      body: Container(
        color: Colors.white,
        padding: EdgeInsets.only(left:20,right:20),
        child: Column(

          children: [
            SizedBox(
              height: 40,
            ),
            Text("REGÍSTRATE",
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w600,
                  fontSize: 30
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text("¡Empieza a reservar tu cita desde ahora!",
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w300,
                  fontSize: 15
              ),),
            SizedBox(
              height: 15,
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(

                  width: 150,
                  child: Divider(
                    color: Colors.black,
                  ),
                ),
                Text("  O  "),
                SizedBox(
                  width: 150,
                  child: Divider(
                    color: Colors.black,
                  ),
                ),
              ],
            ),


            Container(
              padding: EdgeInsets.only(left:20,right:20),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
              ),

              child: Column(
                children: [
                  SizedBox(
                    height: 40,
                  ),
                  TextFormField(
                    controller: emailController,
                    keyboardType: TextInputType.text,
                    style: TextStyle(
                        fontSize: 14
                    ),
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(left:28,right: 28,top: 15,bottom: 15),
                      hintText: "Nombre completo*",
                      hintStyle: TextStyle(color: Color(0xFFA8A8A8)),
                      filled: true,
                      fillColor: Color(0xFFF4FDFE),
                      enabledBorder: OutlineInputBorder(
                        // width: 0.0 produces a thin "hairline" border
                        borderSide: BorderSide(color: Color.fromRGBO(215, 215, 215, 1), width: 1),
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: Text(
                      "*obligatorio",
                      textAlign: TextAlign.left,
                      style: TextStyle(fontSize: 11,color: Colors.grey),
                    ),
                  ),

                  SizedBox(
                    height: 20,
                  ),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        width: 120,
                        child:  TextFormField(
                          controller: docController,
                          keyboardType: TextInputType.text,
                          style: TextStyle(
                              fontSize: 14
                          ),
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(left:28,right: 28,top: 15,bottom: 15),
                            hintText: "Documento*",
                            hintStyle: TextStyle(color: Color(0xFFA8A8A8)),
                            filled: true,
                            fillColor: Color(0xFFF4FDFE),
                            enabledBorder: OutlineInputBorder(
                              // width: 0.0 produces a thin "hairline" border
                              borderSide: BorderSide(color: Color.fromRGBO(215, 215, 215, 1), width: 1),
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      SizedBox(
                        width: 180,
                        child:  TextFormField(
                          controller: docController,
                          keyboardType: TextInputType.text,
                          style: TextStyle(
                              fontSize: 14
                          ),
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(left:28,right: 28,top: 15,bottom: 15),
                            hintText: "Número*",
                            hintStyle: TextStyle(color: Color(0xFFA8A8A8)),
                            filled: true,
                            fillColor: Color(0xFFF4FDFE),
                            enabledBorder: OutlineInputBorder(
                              // width: 0.0 produces a thin "hairline" border
                              borderSide: BorderSide(color: Color.fromRGBO(215, 215, 215, 1), width: 1),
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                        ),
                      ),


                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    controller: emailController,
                    keyboardType: TextInputType.text,
                    style: TextStyle(
                        fontSize: 14
                    ),
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(left:28,right: 28,top: 15,bottom: 15),
                      hintText: "Celular*",
                      hintStyle: TextStyle(color: Color(0xFFA8A8A8)),
                      filled: true,
                      fillColor: Color(0xFFF4FDFE),
                      enabledBorder: OutlineInputBorder(
                        // width: 0.0 produces a thin "hairline" border
                        borderSide: BorderSide(color: Color.fromRGBO(215, 215, 215, 1), width: 1),
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: Text(
                      "*obligatorio",
                      textAlign: TextAlign.left,
                      style: TextStyle(fontSize: 11,color: Colors.grey),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    controller: emailController,
                    keyboardType: TextInputType.text,
                    style: TextStyle(
                        fontSize: 14
                    ),
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(left:28,right: 28,top: 15,bottom: 15),
                      hintText: "Correo Electrónico*",
                      hintStyle: TextStyle(color: Color(0xFFA8A8A8)),
                      filled: true,
                      fillColor: Color(0xFFF4FDFE),
                      enabledBorder: OutlineInputBorder(
                        // width: 0.0 produces a thin "hairline" border
                        borderSide: BorderSide(color: Color.fromRGBO(215, 215, 215, 1), width: 1),
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: Text(
                      "*obligatorio",
                      textAlign: TextAlign.left,
                      style: TextStyle(fontSize: 11,color: Colors.grey),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    controller: emailController,
                    keyboardType: TextInputType.text,
                    style: TextStyle(
                        fontSize: 14
                    ),
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(left:28,right: 28,top: 15,bottom: 15),
                      hintText: "Contraseña*",
                      hintStyle: TextStyle(color: Color(0xFFA8A8A8)),
                      filled: true,
                      fillColor: Color(0xFFF4FDFE),
                      enabledBorder: OutlineInputBorder(
                        // width: 0.0 produces a thin "hairline" border
                        borderSide: BorderSide(color: Color.fromRGBO(215, 215, 215, 1), width: 1),
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: Text(
                      "*obligatorio",
                      textAlign: TextAlign.left,
                      style: TextStyle(fontSize: 11,color: Colors.grey),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    controller: emailController,
                    keyboardType: TextInputType.text,
                    style: TextStyle(
                        fontSize: 14
                    ),
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(left:28,right: 28,top: 15,bottom: 15),
                      hintText: "Repetir contraseña*",
                      hintStyle: TextStyle(color: Color(0xFFA8A8A8)),
                      filled: true,
                      fillColor: Color(0xFFF4FDFE),
                      enabledBorder: OutlineInputBorder(
                        // width: 0.0 produces a thin "hairline" border
                        borderSide: BorderSide(color: Color.fromRGBO(215, 215, 215, 1), width: 1),
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: Text(
                      "*obligatorio",
                      textAlign: TextAlign.left,
                      style: TextStyle(fontSize: 11,color: Colors.grey),
                    ),
                  ),

                  SizedBox(
                    height: 40,
                  ),
                  FlatButton(

                    color:  Color(0xFF03BEDF),
                    minWidth: 220,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25.0),
                        side: BorderSide(color: Color.fromRGBO(255, 255, 255, 1))
                    ),
                    onPressed: (){

                    },


                    child:Padding(
                      padding: EdgeInsets.only(top: 13,bottom: 13),
                      child: Text(
                        "REGISTRARSE",
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),

                  SizedBox(
                    height: 20,
                  )
                ],
              ),
            ),


          ],
        ),

      ),
    );

  }
}
