import 'package:flutter/material.dart';


class MyLogin extends StatefulWidget {
  @override
  _MyLogin createState() => _MyLogin();
}

class _MyLogin extends State<MyLogin> {
  var emailController = TextEditingController(text: "");
  var passController = TextEditingController(text: "");
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          alignment: Alignment.center,
          color: Colors.white,
          child: Column(
            children: [
              SizedBox(
                height: 100,
              ),
              Image.asset(
                "assets/images/logo.png",
                alignment: Alignment.topCenter,
                width: 250,
              ),
              SizedBox(
                height: 20,
              ),
              Text("¡Visualiza, conecta con un emprendedor  y pide un servicio y/o producto!",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 13
                ),
              ),
              SizedBox(
                height: 60,
              ),
              SizedBox(
                width: 300,
                child:  TextFormField(
                  controller: emailController,
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(left:28,right: 28,top: 15,bottom: 15),
                    hintText: "Correo",
                    hintStyle: TextStyle(color: Color(0xFFA8A8A8)),
                    filled: true,
                    fillColor: Color(0xFFF4FDFE),
                    enabledBorder: OutlineInputBorder(
                      // width: 0.0 produces a thin "hairline" border
                      borderSide: BorderSide(color: Color.fromRGBO(215, 215, 215, 1), width: 1),
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),

              SizedBox(
                width: 300,
                child: TextFormField(
                  controller: passController,
                  keyboardType: TextInputType.text,
                  obscureText: true,
                  decoration: InputDecoration(

                    contentPadding: EdgeInsets.only(left:28,right: 28,top: 15,bottom: 15),
                    hintText: "Contraseña",
                    hintStyle: TextStyle(color: Color(0xFFA8A8A8)),
                    filled: true,
                    fillColor: Color(0xFFF4FDFE),
                    enabledBorder: OutlineInputBorder(
                      // width: 0.0 produces a thin "hairline" border
                      borderSide: BorderSide(color: Color.fromRGBO(215, 215, 215, 1), width: 1),
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              FlatButton(
                color:  Color(0xFF03BEDF),
                minWidth: 200,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25.0),
                    side: BorderSide(color: Color.fromRGBO(255, 255, 255, 1))
                ),
                onPressed: (){

                },
                child:Padding(
                  padding: EdgeInsets.only(top: 13,bottom: 13),
                  child: Text(
                    "Entrar",
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),

            ],
          )
      ),
    );

  }
}