import 'package:flutter/material.dart';


class MyMoment extends StatefulWidget {
  @override
  _MyMomentState createState() => _MyMomentState();
}

class _MyMomentState extends State<MyMoment> {
  var emailController = TextEditingController(text: "");
  var passController = TextEditingController(text: "");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.blueAccent,
        padding: EdgeInsets.only(left:20,right:20),
        child: Column(
          children: [
            Image.network("https://upload.wikimedia.org/wikipedia/commons/thumb/f/f9/Google_Lens_-_new_logo.png/600px-Google_Lens_-_new_logo.png",
              width: 250,),
            Text("APP",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w300,
                  fontSize: 50
              ),),
            SizedBox(
              height: 15,
            ),
            Container(
              padding: EdgeInsets.only(left:20,right:20),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
              ),

              child: Column(
                children: [
                  SizedBox(
                    height: 40,
                  ),
                  TextFormField(
                    controller: emailController,
                    keyboardType: TextInputType.emailAddress,
                    style: TextStyle(
                        fontSize: 14
                    ),
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(left:28,right: 28,top: 15,bottom: 15),
                      hintText: "Correo electrónico",
                      hintStyle: TextStyle(color: Color(0xFFA8A8A8)),
                      filled: true,
                      fillColor: Colors.white,
                      enabledBorder: OutlineInputBorder(
                        // width: 0.0 produces a thin "hairline" border
                        borderSide: BorderSide(color: Color.fromRGBO(215, 215, 215, 1), width: 1),
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    controller: passController,
                    keyboardType: TextInputType.emailAddress,
                    obscureText:true,
                    style: TextStyle(
                        fontSize: 14
                    ),
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(left:28,right: 28,top: 15,bottom: 15),
                      hintText: "Contraseña",

                      hintStyle: TextStyle(color: Color(0xFFA8A8A8)),
                      filled: true,
                      fillColor: Colors.white,
                      enabledBorder: OutlineInputBorder(
                        // width: 0.0 produces a thin "hairline" border
                        borderSide: BorderSide(color: Color.fromRGBO(215, 215, 215, 1), width: 1),
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  FlatButton(
                    color:  Colors.blueAccent,
                    minWidth: 100,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25.0),
                        side: BorderSide(color: Color.fromRGBO(255, 255, 255, 1))
                    ),
                    onPressed: (){

                    },


                    child:Padding(
                      padding: EdgeInsets.only(top: 13,bottom: 13),
                      child: Text(
                        "LOGIN",
                        style: TextStyle(
                          fontSize: 15,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  FlatButton(
                    color:  Colors.white,
                    minWidth: 100,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25.0),
                        side: BorderSide(color: Color.fromRGBO(255, 255, 255, 1))
                    ),
                    onPressed: ()=>{

                    },
                    child:Padding(
                      padding: EdgeInsets.only(top: 13,bottom: 13),
                      child: Text(
                        "REGISTER",
                        style: TextStyle(

                            fontSize: 15,
                            color: Colors.black,
                            fontWeight: FontWeight.w400
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  )
                ],
              ),
            ),


          ],
        ),

      ),
    );

  }
}
